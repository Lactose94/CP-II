mod ising2D;
mod file_handeling;

use serde::{Deserialize, Serialize};
use std::sync::mpsc;
use std::thread;
use std::time::Instant;
use std::fs;
use ndarray::prelude::*;
use crate::ising2D::*;
use crate::file_handeling::*;

#[derive(Serialize, Deserialize, Debug, Clone)]
struct Params {
    lattice_size: usize,
    burn_in: usize,
    ntjob: usize,
    ntskip: usize,
    ntprint: usize,
    initial_config: String
}

static OPTIONS_PATH: &'static str = "options.json";

fn main() {
    // read parameters from options
    let pars_data = fs::read_to_string(&OPTIONS_PATH).expect("Something went wrong reading pars");
    let pars: Params = serde_json::from_str(&pars_data).unwrap();
    println!("{:#?}", pars);

    // calculate one timeseries
    let mut system = model::IsingModelMC2D::new(20, 2.1, "random");
    println!("Burning in for timeseries");
    system.burn_in(20_000_000);
    println!("Burn in finished");
    println!("{}", system);
    println!("Run simulation:");
    system.run(2000, 500, 1, 0);

    println!("Finished simulation.\nSaving to file");
    arrays_to_file("data/m_time.dat", &system.time_series, &system.magnetization);
    println!("Finished saving");

    println!("Starting calculations for temperature graph");
    // calculate for given temperatures
    let (tx, rx) = mpsc::channel::<(usize, f64, std::time::Duration)>();
    let mut handles = Vec::new();

    // temperatures
    let ts: Array1<f64> = array![1.0, 1.5, 1.75, 2.0, 2.1, 2.2, 2.3];
    let mut ms: Array1<f64> = Array1::zeros(ts.len());
    
    // run each temperature parallel
    for i in 0..ts.len() {
        let tx1 = mpsc::Sender::clone(&tx);
        let t = ts[i];
        let loc_pars = pars.clone();
        handles.push(
            thread::spawn(move || {
                let start = Instant::now();
                let mut system = model::IsingModelMC2D::new(loc_pars.lattice_size, t , &loc_pars.initial_config);
                system.burn_in(loc_pars.burn_in);
                println!("Thread {}: burn in finished: {:.2?}",i, start.elapsed());
                // TODO: take more sweeps etc
                system.run(loc_pars.ntjob, loc_pars.ntjob, loc_pars.ntprint, i);

                tx1.send((i, system.avg_m.abs(), start.elapsed())).unwrap();
            })
        );
    }

    let mut ctr = 0;
    for (i, m, time) in rx {
        ctr += 1;
        ms[i] = m;
        println!("{}: Thread {} finished after {:.2?}", ctr, i, time);

        if ctr == 5 {
            break
        }
    }

    arrays_to_file("data/mt.dat", &ts, &ms);
}
