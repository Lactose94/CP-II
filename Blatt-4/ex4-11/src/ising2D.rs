use ndarray::prelude::*;
use rand::Rng;

fn modulo(a: isize, b: usize) -> usize {
    let bu = b as isize;
    (((a % bu) + bu) % bu) as usize
}

fn init_strategy(n: usize, strategy: &str) -> Array2<i8> {
    if strategy == "random" {
        let mut rnd_arr: Array2<i8> = Array::zeros((n, n));
        for i in 0..n {
            for j in 0..n{
                rnd_arr[[i, j]] = 2 * rand::thread_rng().gen_range(0, 2) - 1;
            }
        }
        rnd_arr
    }
    else if strategy == "all_up" {
        Array::ones((n, n))
    }
    else if strategy == "all_down" {
        -Array::ones((n, n))
    }
    else {
        panic!("Unknown strategy in init_config")
    }
}

fn repr_spin(spin: &i8) -> char {
    if spin == &1 {
        '+'
    }
    else if spin == &-1 {
        '-'
    }
    else {
        panic!("spin can only be up or down")
    }
}

pub mod model {
    use super::*;

    #[derive(Default)]
    pub struct IsingModelMC2D {
        n: usize,
        t: f64,
        pub avg_m: f64,
        m: f64,
        energy: f64,
        pub configuration: Array2<i8>,
        exp4: f64,
        exp8: f64,

        // values sampled in runs
        n_attempted: usize,
        n_accepted: usize,
        pub magnetization: Array1<isize>,
        pub magn_per_spin: Array1<f64>,
        pub time_series: Array1<f64>
    }

    impl std::fmt::Display for IsingModelMC2D {
        fn fmt (&self, fmt: &mut std::fmt::Formatter) -> std::result::Result<(), std::fmt::Error> {
            let mut s = String::from("****************************************************************");
            s += "\n** Ising model 2D MC simulation";
            s += &format!("\n- {:>11} = {}", "N", self.n);
            s += &format!("\n- {:>11} = {:.2}", "T", self.t);
            s += &format!("\n- {:>11} = {:.2}", "E", self.energy);
            s += &format!("\n- {:>11} = {:.2}", "m", self.m);
            s += "\n- CONFIG:";
            s += &format!("\n{}", self.config_to_s());
            s += "\n****************************************************************";
            write!(fmt, "{}", s)
        }
    }

    impl IsingModelMC2D {
        pub fn new(n: usize, t: f64, strategy: &str) -> IsingModelMC2D {
            let exp4 = (-4./t).exp();
            let exp8 = (-8./t).exp();

            let configuration = init_strategy(n, strategy);

            IsingModelMC2D {
                n,
                t,
                configuration,
                exp4,
                exp8,
                ..Default::default()
            }
        }

        pub fn config_to_s(&self) -> String {
            let conf_str = self.configuration.map(repr_spin);
            format!("{}", conf_str)
        }

        fn get_m(&self) -> isize {
            let mut mag = 0;
            for i in 0..self.n {
                for j in 0..self.n {
                    mag += self.configuration[[i, j]] as isize;
                }
            }
            mag as isize
            //self.configuration.sum() as isize
        }

        fn do_move(&mut self) {

            self.n_attempted += 1;

            let i = rand::thread_rng().gen_range(0, self.n);
            let j = rand::thread_rng().gen_range(0, self.n);

            let iplus = modulo(i as isize + 1, self.n);
            let iminus = modulo(i as isize - 1, self.n);
            let jplus = modulo(j as isize + 1, self.n);
            let jminus = modulo(j as isize - 1, self.n);

            let u_old = -self.configuration[[i, j]] * (
                self.configuration[[i, jplus]]
                + self.configuration[[i, jminus]]
                + self.configuration[[iminus, j]]
                + self.configuration[[iplus, j]]
            );

            let du = -2 * u_old;

            if du <= 0 {
                self.configuration[[i, j]] *= -1;
                self.n_accepted += 1;
            }
            else {
                let acc_prop: f64 = if du == 4 {
                    self.exp4
                }
                else if du == 8 {
                    self.exp8
                }
                else {
                    panic!("Energy difference should only be 4 or 8 not {}", du)
                };

                let ran: f64 = rand::thread_rng().gen_range(0.0, 1.);

                if ran < acc_prop {
                    self.configuration[[i, j]] *= -1;
                    self.n_accepted += 1;
                }
            }


        }

        pub fn burn_in(&mut self, burn_in_period: usize) {
            for _ in 0..burn_in_period {
                self.do_move();
            }
        }

        pub fn run(&mut self, n_sweeps: usize, n_skip: usize, n_out: usize, id: usize) {
            self.magnetization = Array1::zeros(n_sweeps);
            self.magn_per_spin = Array1::zeros(n_sweeps);
            self.time_series = Array1::range(0., n_sweeps as f64, 1.);
            for i in 0.. n_sweeps {
                for _ in 0..self.n.pow(2) * n_skip {
                    self.do_move();
                }

                let magnetization = self.get_m();
                if n_out > 0  && i % n_out == 0 {
                    println!("Thread {}: {:>2} = {:5}\t{:>2} = {:+5}\t {:3} = {:.2}", id, "t", i, "M", magnetization, "acc", self.get_acc());
                }
                let m_per_spin = magnetization as f64 / self.n.pow(2) as f64;
                self.avg_m += m_per_spin;

                self.magnetization[i] = magnetization;
                self.magn_per_spin[i] = m_per_spin;
            }
            self.avg_m *= 1. / n_sweeps as f64;
        }

        pub fn get_acc(&self) -> f64 {
            if self.n_attempted > 0 {
                self.n_accepted as f64 / self.n_attempted as f64
            }
            else {
                0.
            }
        }
    }

}