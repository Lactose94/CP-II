use std::io::Write;
use std::fs;
use rand::Rng;
#[macro_use]
extern crate filehandler;
#[macro_use]
use itertools::izip;

fn move_1(x: f64, delta: f64) -> f64 {
    let xi = rand::thread_rng().gen_range(-delta, delta);
    x + xi
}

fn gen_phi(delta: f64) -> f64 {
    let phi = rand::thread_rng().gen_range(1., 1. + delta);
    let prob = rand::thread_rng().gen_range(0., 1.);
    if prob < 0.5 {
        1. / phi
    }
    else {
        phi
    }
}

fn move_2(x: f64, delta: f64) -> f64 {
    let phi = rand::thread_rng().gen_range(1., 1. + delta);
    let prob = rand::thread_rng().gen_range(0., 1.);
    if prob < 0.5 {
        x / phi
    }
    else {
        phi * x
    }
}

fn accept_1(x: f64, y: f64, exp_beta_eps: f64) -> bool {
    if y > 2. || y < 0. {
        false
    }
    else if x <= 2. && x > 1. && y >= 0. && y < 1. {
        let acc_prob = rand::thread_rng().gen_range(0., 1.);
        if acc_prob < exp_beta_eps {
        true
        }
        else {
            false
        }
    }
    else {
        true
    }
}

// Wrong criterion, look at moodle to see correct solution
fn accept_2(x: f64, y: f64, eps: f64, beta: f64) -> bool {
    let delta_u = eps * (char_fc(y, 0., 1.) - char_fc(x, 0., 1.));
    let exp_delta_u = (-beta * delta_u).exp();
    let pacc = exp_delta_u * y / x;
    if y < 0. || y > 2. {
        false
    }
    else if pacc > 1. {
        true
    }
    else {
        let acc_prob = rand::thread_rng().gen_range(0., 1.);
        if acc_prob < pacc {
            true
        }
        else {
            false
        }
    }
}

fn char_fc(x: f64, lower: f64, upper: f64) -> f64 {
    if x >= lower && x <= upper {
        1.
    }
    else {
        0.
    }
}

#[derive(Debug)]
struct Intervall {
    lower: f64,
    upper: f64
}

impl Intervall {
    fn new(lower: f64, upper: f64) -> Intervall {
        if lower > upper {
            panic!("Upper boundary has to be smaller then lower")
        }
        Intervall {
            upper,
            lower
        }
    }

    fn is_in(&self, value: f64) -> bool {
        self.lower <= value && self.upper > value
    }
}

#[derive(Debug)]
struct Histogram {
    bins: Vec<(Intervall, usize)>,
    upper: f64,
    lower: f64,
    nr_bins: usize,
    data_count: usize
}

impl Histogram {
    fn new(lower: f64, upper: f64, nr_bins: usize) -> Histogram {
        if lower > upper {
            panic!("Upper boundary has to be smaller then lower")
        }

        let step_size = (upper - lower) / nr_bins as f64;
        let mut bins = Vec::new();
        for i in 0..nr_bins {
            bins.push(
                (
                    Intervall::new(lower + i as f64 * step_size, lower + (i + 1) as f64 * step_size),
                    0
                )
            )
        }

        Histogram {
            bins,
            upper,
            lower,
            nr_bins,
            data_count: 0
        }
    }

    fn count(&mut self, data: f64) -> Result<(), &'static str>{
        for i in 0..self.nr_bins {
            if self.bins[i].0.is_in(data) {
                self.bins[i].1 += 1;
                self.data_count += 1;
                return Ok(())
            }
        }
        Err("Could not sort into any bin")
    }
}

fn main() {
    const N: usize = 1_000_000;
    const DELTA: f64 = 1.;
    let beta: f64 = 1.;
    let eps: f64 = 1.;
    let exp_beta_eps = (- beta * eps).exp();

    let mut phis: Vec<f64> = vec![0.0; N];
    for i in 0..N {
        phis[i] = gen_phi(DELTA);
    }

    print_iter!("data/phi.dat", phis);

    let mut x1s = vec![0.0; N];
    let mut x1 = rand::thread_rng().gen_range(0., 2.);
    let mut x2s = vec![0.0; N];
    let mut x2 = rand::thread_rng().gen_range(0., 2.);
    let mut x3s = vec![0.0; N];
    let mut x3 = rand::thread_rng().gen_range(0., 2.);
    for i in 0..N {
        let y1 = move_1(x1, DELTA);
        let y2 = move_2(x2, DELTA);
        let y3 = move_2(x3, DELTA);

        if accept_1(x1, y1, exp_beta_eps) {
            x1s[i] = y1;
            x1 = y1;
        }
        else {
            x1s[i] = x1;
        }

        if accept_2(x2, y2, eps, beta) {
            x2s[i] = y2;
            x2 = y2;
        }
        else {
            x2s[i] = x2;
        }

        if accept_1(x3, y3, exp_beta_eps) {
            x3s[i] = y3;
            x3 = y3;
        }
        else {
            x3s[i] = x3;
        }
    }

    print_iter!("data/full1.dat", x1s);
    print_iter!("data/full2.dat", x2s);
    print_iter!("data/wrong-crit.dat", x3s);
}
