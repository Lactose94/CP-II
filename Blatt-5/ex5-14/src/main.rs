use rand::Rng;
use std::f64::consts::PI;
use std::time::Instant;
use std::fs;
use std::io;
use std::io::Write;
use std::io::BufWriter;
use std::env;

#[cfg(test)]
mod tests {
    use approx::relative_eq;
    use super::*;
        #[test]
        fn absolut() {
            let v = vec![0.; 3];
            assert_eq!(abs(&v), 0.);
            let v = vec![1., 0., 0.];
            assert_eq!(abs(&v), 1.);
            let v = vec![1.; 3];
            relative_eq!(abs(&v), f64::sqrt(3.));
        }

        #[test]
        fn test_div() {
            let v = vec![1., 0., 0.];
            assert_eq!(div(v, 2.), vec![0.5, 0., 0.]);
        }

        #[test]
        fn test_1() {
            let v = generate_1();
            relative_eq!(abs(&v), 1.);
        }

        #[test]
        fn test_5() {
            let v = generate_5();
            relative_eq!(abs(&v), 1.);
        }
}

fn div(vec: Vec<f64>, rhs: f64) -> Vec<f64> {
    if rhs == 0. {
        panic!("Zero division")
    }

    vec.iter().map(|x| x / rhs).collect()
}

fn abs(vec: &Vec<f64>) -> f64 {
    vec.iter()
        .map(|x| x * x)
        .sum::<f64>()
        .sqrt()
}

fn generate_1() -> Vec<f64>{
    let mut v: Vec<f64> = vec![0.0; 3];
    for i in 0..3 {
        v[i] = rand::thread_rng().gen_range(-1., 1.);
    }

    let abs_v = abs(&v);
    div(v, abs_v)
}

fn generate_2() -> Vec<f64> {
    let mut v: Vec<f64> = vec![0.0; 3];
    let mut abs_v;
    loop {
        for i in 0..3 {
            v[i] = rand::thread_rng().gen_range(-1., 1.);
        }
        abs_v = abs(&v);
        if abs_v <= 1.  {
            break;
        }
        else {
            vec![0.0; 3];
        }
    }

    div(v, abs_v)
}

fn generate_3() -> Vec<f64> {
    let phi = rand::thread_rng().gen_range(0., 2. * PI);
    let theta = rand::thread_rng().gen_range(0., PI);

    vec![
        phi.cos() * theta.sin(),
        phi.sin() * theta.sin(),
        theta.cos()
        ]
}

fn generate_4() -> Vec<f64> {
    let phi = rand::thread_rng().gen_range(0., 2. * PI);
    let cos_theta: f64 = rand::thread_rng().gen_range(-1., 1.);
    // does not work, since you do not get the negative values -> Does not need to since sin theta > 0 on [0, pi]
    let sin_theta = (1. - cos_theta.powf(2.)).sqrt();
    vec![
        phi.cos() * sin_theta,
        phi.sin() * sin_theta,
        cos_theta
        ]
}

fn generate_5() -> Vec<f64> {
    let mut v: Vec<f64> = vec![0.0; 4];
    let mut abs_v;
    loop {
        for i in 0..4 {
            v[i] = rand::thread_rng().gen_range(-1., 1.);
        }
        abs_v = abs(&v);
        if abs_v <= 1. {
            break;
        }
        else {
            vec![0.0; 4];
        }
    }

    let x = vec![
        2. * (v[1] * v[3] + v[0] * v[2]),
        2. * (v[2] * v[3] - v[0] * v[1]),
        v[0].powi(2) - v[1].powi(2) - v[2].powi(2) + v[3].powi(2)
    ];

    div(x, abs_v.powi(2))
}

fn get_angles(vec: Vec<f64>) -> (f64, f64) {
    let phi = (vec[1] / vec[0] ).atan();
    let theta = (vec[2] / abs(&vec)).atan();

    (phi, theta)
}

fn run_method(name: &str, n: usize, method: impl Fn() -> Vec<f64>) {
    print!("{}: ", name.to_uppercase());
    io::stdout().flush().unwrap();
    let file_angle = format!("data/angle-{}.dat", name);
    let file_3d = format!("data/3d-{}.dat", name);
    let mut out_angle_buf = BufWriter::new(fs::File::create(file_angle).expect("could not create"));
    let mut out_3d_buf = BufWriter::new(fs::File::create(file_3d).expect("could not create"));
    let start = Instant::now();
    for _ in 0..n {
        let v = method();
        let line_3d = format!("{}\t{}\t{}\n", v[0], v[1], v[2]);
        out_3d_buf
            .write_all(line_3d.as_bytes())
            .expect("could no write");
        let (phi, theta) = get_angles(v);
        let line_angle = format!("{}\t{}\n", phi, theta);
        out_angle_buf
            .write_all(line_angle.as_bytes())
            .expect("could not write");
    }
    println!("finished after {:.2?}", start.elapsed());
    out_3d_buf.flush().unwrap();
    out_angle_buf.flush().unwrap();
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let n: usize = args[1].parse().unwrap();
    let methods = [generate_1, generate_2, generate_3, generate_4, generate_5];

    for (i, method) in methods.iter().enumerate() {
        let name = format!("method{}", i+1);
        run_method(&name, n, method);
    }
}
