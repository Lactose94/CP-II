use std::fs;
use std::io::Write;

use rand::Rng;
use ndarray::prelude::*;
use ndarray_linalg::solve::Solve;
extern crate openblas_src;

fn calc_e(u: &Array1<f64>) -> f64 {
    u.iter().map(|x| x * x * 0.5).sum()
}

fn random_velocity(beta: f64) -> f64 {
    let r: f64 = rand::thread_rng().gen_range(0., 1.);
    let v = (-2. * 1. / beta * r.ln()).sqrt();
    let phi: f64 = rand::thread_rng().gen_range(0., 2. * std::f64::consts::PI);
    v * (phi).sin() / beta.sqrt()
}

#[derive(Debug)]
struct Integrator {
    delta_t: f64,
    n_iter: usize,
    ts: Array1<f64>,
    us: Array2<f64>,
    es: Array1<f64>
}

impl Integrator {
    fn new(delta_t: f64, n_iter: usize) -> Integrator {
        let ts = Array1::zeros(n_iter);
        let us = Array2::zeros((n_iter, 2));
        let es = Array1::zeros(n_iter);

        Integrator {
            delta_t,
            n_iter,
            ts,
            us,
            es
        }
    }

    fn euler_fwd(&mut self, u0: &Array1<f64>) {
        let D: Array2<f64> = self.delta_t * array![[0., 1.], [-1., 0.]];
        self.us.slice_mut(s![0, ..]).assign(&u0);
        self.es[0] = calc_e(&u0);
        for i in 1..self.n_iter {
            print!("{}/{}\r", i, self.n_iter);
            self.ts[i] = i as f64 * self.delta_t;
            let unp1 = {
                    let un = self.us.slice(s![i-1, ..]);
                    &un + &D.dot(&un)
            };
            self.us.slice_mut(s![i, ..]).assign(&unp1);
            self.es[i] = calc_e(&unp1);
        }

    }

    fn euler_impl(&mut self, u0: &Array1<f64>) {
        let D: Array2<f64> = self.delta_t * array![[0., 1.], [-1., 0.]];
        self.us.slice_mut(s![0, ..]).assign(&u0);
        self.es[0] = calc_e(&u0);
        for i in 1..self.n_iter {
            print!("{}/{}\r", i, self.n_iter);
            self.ts[i] = i as f64 * self.delta_t;
            let unp1 = {
                    let un = self.us.slice(s![i-1, ..]);
                    let B: Array2<f64> = Array2::eye(2) - &D;
                    B.solve(&un).unwrap()
            };
            self.us.slice_mut(s![i, ..]).assign(&unp1);
            self.es[i] = calc_e(&unp1);
        }
    }

    fn leap_frog(&mut self, u0: &Array1<f64>) {
        let D: Array2<f64> = 2. * self.delta_t * array![[0., 1.], [-1., 0.]];
        self.us.slice_mut(s![0, ..]).assign(&u0);
        self.es[0] = calc_e(&u0);
        // use one step of Euler forwards to get two values to start with
        self.ts[1] = self.delta_t;
        let unp1 = {
                let un = self.us.slice(s![0, ..]);
                &un + &D.dot(&un)
        };
        self.us.slice_mut(s![1, ..]).assign(&unp1);
        self.es[1] = calc_e(&unp1);

        for i in 2..self.n_iter {
            print!("{}/{}\r", i, self.n_iter);
            self.ts[i] = i as f64 * self.delta_t;
            let unp1 = {
                    let un = self.us.slice(s![i-1, ..]);
                    let unmin1 = self.us.slice(s![i-2, ..]);
                    &unmin1 +  &D.dot(&un)
            };
            self.us.slice_mut(s![i, ..]).assign(&unp1);
            self.es[i] = calc_e(&unp1);
        }
    }

    fn vel_verlet(&mut self, u0: &Array1<f64>) {
        self.us.slice_mut(s![0, ..]).assign(&u0);
        self.es[0] = calc_e(&u0);
        for i in 1..self.n_iter {
            self.ts[i] = i as f64 * self.delta_t;
            let qnp1 = (1. - self.delta_t.powi(2)/2.) * self.us[[i-1, 0]] + self.us[[i-1, 1]] * self.delta_t;
            let vnp1 = self.us[[i-1, 1]] - self.delta_t * (self.us[[i-1, 0]] + qnp1) / 2.;
            let unp1 = array![qnp1, vnp1];
            self.us.slice_mut(s![i, ..]).assign(&unp1);
            self.es[i] = calc_e(&unp1)
        }
    }


    fn vel_verlet_redraw(&mut self, u0: &Array1<f64>, beta: f64, steps: usize) {
        self.us.slice_mut(s![0, ..]).assign(&u0);
        self.es[0] = calc_e(&u0);
        let mut ctr: usize = 0;
        for i in 1..self.n_iter {
            ctr += 1;
            self.ts[i] = i as f64 * self.delta_t;
            let qnp1 = (1. - self.delta_t.powi(2)/2.) * self.us[[i-1, 0]] + self.us[[i-1, 1]] * self.delta_t;
            let vnp1 = if ctr % steps == 0 {
                ctr = 0;
                random_velocity(beta)
            }
            else {
                self.us[[i-1, 1]] - self.delta_t * (self.us[[i-1, 0]] + qnp1) / 2.
            };
            let unp1 = array![qnp1, vnp1];
            self.us.slice_mut(s![i, ..]).assign(&unp1);
            self.es[i] = calc_e(&unp1)
        }
    }

    fn get_data(self) -> (Array1<f64>, Array2<f64>, Array1<f64>) {
        (self.ts, self.us, self.es)
    }

    fn write_to(self, path: &str) -> Result<(), std::io::Error> {
        let mut file = fs::File::create(path)?;
        for i in 0..self.n_iter {
            // write as      t   q   p   e
            let s = format!("{}\t{}\t{}\t{}\n", self.ts[i], self.us[[i, 0]], self.us[[i, 1]], self.es[i]);
            file.write_all(s.as_bytes())?;
        }
        Ok(())
    }
}

fn main() {
    const DELTA_T: f64 = 0.003;
    const N: usize = 100_000;
    let u0: Array1<f64> = array![1., 0.];

    let mut euler_fwd = Integrator::new(DELTA_T, N);
    euler_fwd.euler_fwd(&u0);
    euler_fwd.write_to("data/euler_fwd.dat").unwrap();

    let mut euler_impl = Integrator::new(DELTA_T, N);
    euler_impl.euler_impl(&u0);
    euler_impl.write_to("data/euler_impl.dat").unwrap();

    let mut leap_frog = Integrator::new(DELTA_T, N);
    leap_frog.leap_frog(&u0);
    leap_frog.write_to("data/leap_frog.dat").unwrap();

    let mut vel_verlet = Integrator::new(DELTA_T, N);
    vel_verlet.vel_verlet(&u0);
    vel_verlet.write_to("data/vel_verlet.dat").unwrap();

    let mut vel_verlet_redraw = Integrator::new(0.03, N);
    vel_verlet_redraw.vel_verlet_redraw(&u0, 1., 10);
    vel_verlet_redraw.write_to("data/vel_verlet_redraw.dat").unwrap();
}
