use ndarray::prelude::*;
use filehandler;


#[cfg(test)]
mod tests {
    use super::*;
        #[test]
        fn integration() {
            let weights = trapz_weights(5, 0.25);
            let vals: Array1<f64> = Array1::ones(5);

            assert_eq!(1f64, weights.dot(&vals));

            let weights = trapz_weights(5, 1.);
            assert_eq!(4f64, weights.dot(&vals));

            let fx: Array1<f64> = Array1::linspace(0., 1., 4);
            let dx = fx[1] - fx[0];
            let weights = trapz_weights(4, dx);
            assert_eq!(0.5, weights.dot(&fx));
        }
    }


/// Function that calculates the weights for the trapezoidal rule
/// given the stepsize/intervallsize and the nr of datapoints
fn trapz_weights(n: usize, step: f64) -> Array1<f64> {
    let mut weights: Array1<f64> = Array1::ones(n);
    weights[0] = 0.5;
    weights[n-1] = 0.5;
    weights * step
}

fn main() {
    // read in time and velocities
    let t_v0 = filehandler::read_to_array("velocity_trajs/v_0.dat").unwrap();
    let t_v1 = filehandler::read_to_array("velocity_trajs/v_1.dat").unwrap();
    let t_v2 = filehandler::read_to_array("velocity_trajs/v_2.dat").unwrap();
    let data = [t_v0, t_v1, t_v2];

    for (j, traj) in data.iter().enumerate() {
        println!("Run for v{}:", j);
        // get nr of datapoints
        let n_data = {
            let mn = traj.shape();
            mn[0]
        };

        let cutoff = 500;
        // veloctiy autocorrelation function
        let mut vacf: Array1<f64> = Array1::zeros(cutoff);

        for i in 0..cutoff {
            print!("i={}\r", i);
            let v_i = traj.slice(s![..n_data-i, 1..]);
            let v_k = traj.slice(s![i.., 1..]);
            vacf[i] = (&v_i * &v_k).sum_axis(Axis(1)).mean().unwrap();
        }

        //calculate timestep
        let t_step: f64 = traj[[1, 0]] - traj[[0,0]];
        // make weights
        let weights = trapz_weights(cutoff, t_step);
        // calculate diffusion coefficient
        let d: f64 = weights.dot(&vacf) / 3.;
        println!("\tD_{} = {:.5}", j, d);

        // write vacf to file
        let t = traj.slice(s![..cutoff, 0]).to_owned();
        let path = format!("data/vacf-{}.dat", j);
        filehandler::arrays_to_file(&path, &t, &vacf).expect("error while writing to file");

        // calculate r^2(t) from the diffusion constant

        let t = traj.slice(s![.., 0]).to_owned();
        let r2 =  6. * d * &t;
        // write mean squared distance to file
        let path = format!("data/r2-v{}.dat", j);
        filehandler::arrays_to_file(&path, &t, &r2).expect("error while writing to file");
    }
}
