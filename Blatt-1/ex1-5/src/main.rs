use std::fs;
use std::io::Write;

fn trapez(x: &Vec<f64>, fx: &Vec<f64>) -> f64 {
    let n: usize = if x.len() != fx.len() {
        panic!("different size of points and fct values");
    } else {
        x.len()
    };

    let mut integral: f64 = 0.0;
    for i in 0..(n - 1) {
        integral += (x[i + 1] - x[i]) * (fx[i] + fx[i + 1]) / 2.0;
    }
    integral
}

fn read_log(filename: &str) -> Vec<Vec<f64>> {
    let file_content =
        fs::read_to_string(filename).expect("Something went wrong wile reading the file!");
    let lines: Vec<&str> = file_content.split("\n").collect();
    let nr_rows = lines.len() - 1;
    let nr_cols: usize = {
        let line: Vec<&str> = lines[0].split_whitespace().collect();
        line.len()
    };

    let mut result: Vec<Vec<f64>> = Vec::new();
    for _i in 0..nr_cols {
        result.push(vec![0.0; nr_rows])
    }
    for (i, line) in lines.iter().enumerate() {
        if line == &"" {
            continue;
        }
        let line_content: Vec<&str> = line.split_whitespace().collect();
        for j in 0..nr_cols {
            result[j][i] = line_content[j]
                .parse()
                .expect("could not parse col {j}; row {i}");
        }
    }
    result
}

fn main() {
    const FILES: [&str; 2] = [
        "/home/lactose/Programs/CP-II/Blatt-1/ex1-5/tgr1167.dat",
        "/home/lactose/Programs/CP-II/Blatt-1/ex1-5/tgr1230.dat",
    ];
    // just a comment
    for file in FILES.iter() {
        let gr: Vec<Vec<f64>> = read_log(file);
        let mut nr: Vec<f64> = vec![0.0; gr[0].len()];
        for i in 1..gr[0].len() {
            nr[i] = trapez(&gr[0][..i].to_vec(), &gr[1][..i].to_vec());
        }
        // Forgot to add the density!!

        let path_out = file.replace("/tgr", "/tnr");
        let mut file_out = fs::File::create(&path_out).expect("could not create");
        println!("Written output to {}", path_out);
        for (n, s) in gr[0].iter().zip(nr.iter()) {
            let line = format!("{}\t{}\n", n, s);
            file_out
                .write_all(line.as_bytes())
                .expect("could not write");
        }
    }
}
