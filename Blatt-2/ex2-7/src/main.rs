use std::fmt::Display;
use ndarray::prelude::*;
use std::fs;
use std::io::Write;
use std::env;
use std::io;
use std::time::Instant;


fn read_to_array(filepath: &str) -> Array<f64, Ix2> {
    let file_content =
        fs::read_to_string(filepath).expect("Something went wrong wile reading the file!");
    let lines = file_content.split("\n");
    let lines: Vec<&str> = lines.filter(|&line| line != "").collect::<Vec<&str>>();

    let nr_rows = lines.len();
    let nr_cols: usize = {
        let line: Vec<&str> = lines[0].split_whitespace().collect();
        line.len()
    };

    let mut content: Vec<f64> = vec![0.0; nr_rows * nr_cols];

    for (i, line) in lines.iter().enumerate() {
        let line_content = line.split_whitespace();
        for (j, cont) in line_content.enumerate() {
            content[i * nr_cols + j] = cont.parse::<f64>().unwrap();
        }
    }
    Array::from_shape_vec((nr_rows, nr_cols), content).unwrap()
}

fn make_weights(n: usize, step: f64) -> Array1<f64> {
    let mut weights: Array1<f64> = Array1::ones(n);
    weights[0] = 0.5;
    weights[1] = 0.5;
    weights * step
}

fn arrays_to_file<T>(filepath: &str, array1: &Array1<T>, array2: &Array1<T>)
    where
        T: Display
{
    assert_eq!(array1.len(), array2.len());
    let mut file_out = fs::File::create(&filepath).expect("could not create");
    for (x, y) in array1.iter().zip(array2.iter()) {
        let line = format!("{}\t{}\n", x, y);
        file_out
            .write_all(line.as_bytes())
            .expect("could not write");
    }
}

fn main() {
    // constants
    const PATH: &str = "/home/lactose/Programs/CP-II/Blatt-2/ex2-6/g1000.dat";
    const RHO: f64 = 0.84;
    const PI: f64 = std::f64::consts::PI;
    // this is just a reminder, that we choose LJ units

    // read in nr of k and maximum value
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 {
        println!("supply correct arguments");
        return };
    let n: usize = args[1].parse().unwrap();
    let kmax: f64 = args[2].parse().unwrap();

    // initial data
    print!("initializing data");
    io::stdout().flush().unwrap();
    let start = Instant::now();

    let k_vec = Array::linspace(1e-9, kmax, n);
    let k_step: f64 = k_vec[1] - k_vec[0];

    let vals_in = read_to_array(PATH);
    let g = vals_in.slice(s![.., 1]);
    let r_in = vals_in.slice(s![.., 0]);
    let r_step: f64 = r_in[1 as usize] - r_in[0 as usize];

    let nr_rows = {
        let sl = vals_in.shape();
        sl[0]
    };

    println!("...finished after {:#?}", start.elapsed());

    // calculate sin(kr)
    print!("initializing sin(kr)");
    io::stdout().flush().unwrap();
    let start = Instant::now();

    let ks = k_vec.clone().insert_axis(Axis(1));
    let sinkr: Array2<f64> = {
        let kr: Array2<f64> = ks.dot(&r_in.insert_axis(Axis(0)));
        kr.mapv_into(f64::sin)
    };

    println!("...finished after {:#?}", start.elapsed());

    // calculate s from sin(kr) and g
    print!("calculating S");
    io::stdout().flush().unwrap();
    let start = Instant::now();

    let s: Array1<f64> = {
        let gr_min1 = g.mapv(|x| x - 1.);
        let integrand = &sinkr * &gr_min1 * &r_in;

        let weights = make_weights(nr_rows, r_step);

        1. + 4.0 * PI * RHO * integrand.dot(&weights) / &k_vec
    };

    println!("...finished after {:#?}", start.elapsed());

    // recalculating g from S again
    print!("calculating g from S");
    let start = Instant::now();
    io::stdout().flush().unwrap();

    let g_rec: Array1<f64> = {
        let s_min1 = s.mapv(|x| x - 1.).insert_axis(Axis(1));
        let integrand = sinkr * s_min1 * ks;

        let weights = make_weights(n, k_step);

        let res = integrand.t().dot(&weights) / (2. * PI.powi(2) * RHO);
        1. + res / r_in
    };

    println!("...finished after {:#?}", start.elapsed());

    // writing data to files
    print!("writing to files");
    let start = Instant::now();
    io::stdout().flush().unwrap();

    arrays_to_file("s512.dat", &k_vec, &s);
    arrays_to_file("rec_g512.dat", &r_in.to_owned(), &g_rec);

    println!("...finished after {:#?}", start.elapsed());
}
