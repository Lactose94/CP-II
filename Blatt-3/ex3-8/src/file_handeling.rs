use std::fs;
use std::fmt::Display;
use std::io::Write;
use ndarray::prelude::*;

const N_SWEEPS: usize = 20_000;

#[derive(Debug, Clone)]
pub struct RunTimeVars {
    pub debug_level: usize,
    pub n_sweeps: usize
}

pub fn init_rt_vars(args: &[String]) -> RunTimeVars {
    let debug_level: usize = match args.iter().find(|&var| var.starts_with("debug")) {
        Some(dbg) => {
            let dbg_str = dbg.split("=").last().unwrap();
            dbg_str.parse().unwrap_or_else(
                |err| {
                    println!("Debug level: {} - fall back to default 0", err );
                    0
                }
            )
        }
        None => 0
    };
    let n_sweeps: usize = match args.iter().find(|&var|  var.starts_with("n_sweeps")) {
        Some(sweeps) => {
            let sweeps_str = sweeps.split("=").last().unwrap();
            sweeps_str.parse().unwrap_or_else(
                |err| {
                    println!("n_sweeps: {} - fall back to default {}", err, N_SWEEPS);
                    0
                }
            )
        }
        None => N_SWEEPS
    };
    println!("Run with:\n\tDebug level: {}\n\tn_sweeps: {}", debug_level, n_sweeps);
    RunTimeVars{
        debug_level,
        n_sweeps
    }
}

pub fn arrays_to_file<T, S>(filepath: &str, array1: &Array1<T>, array2: &Array1<S>)
    where
        T: Display,
        S: Display
{
    assert_eq!(array1.len(), array2.len());
    let mut file_out = fs::File::create(&filepath).expect("could not create");
    for (x, y) in array1.iter().zip(array2.iter()) {
        let line = format!("{}\t{}\n", x, y);
        file_out
            .write_all(line.as_bytes())
            .expect("could not write");
    }
}