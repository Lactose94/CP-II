use ndarray::prelude::*;
use std::time::Instant;
use std::sync::mpsc;
use std::thread;

use crate::ising::*;
use crate::file_handeling::{RunTimeVars, arrays_to_file};

// TODO: Create function that just runs a basic simulation to test parameters
pub fn simple_run() -> Result<(), &'static str>  {
    const RT_VARS: RunTimeVars = RunTimeVars {
        debug_level: 1,
        n_sweeps: 200_000_000
    };

    static PARS: Params = Params {
        system_size: 100,
        h_over_j: 0.0,
        init_strategy: "random",
        boundary_conditions: "pbc",
        t: 1.0,
        i_stats: 10,
        i_stats_out: 1_000_000
    };

    let mut system = model::IsingModelMC1D::new(&PARS)?;
    system.run(&RT_VARS, &PARS);
    if system.get_acceptance_rate() > 0.25 {
        println!("!!!!!!!!!!!! WARNING !!!!!!!!!!!!");
        println!("System ist not equilibrated");
        println!("{} is bigger than 0.25", system.get_acceptance_rate());
    }
    println!("{}", system);
    println!("m = {:10}", system.get_m());
    Ok(())
}

pub fn magnetization_of_h() -> Result<(), &'static str> {
    const N_PLOT: usize = 100;

    const RT_VARS: RunTimeVars = RunTimeVars {
        debug_level: 1,
        n_sweeps: 2_000_000
    };

    static PARS: Params = Params {
        system_size: 100,
        h_over_j: 0.0,
        init_strategy: "random",
        boundary_conditions: "pbc",
        t: 1.0,
        i_stats: 10,
        i_stats_out: 0
    };

    let hs: Array1<f64> = Array::linspace(0., 1., N_PLOT);
    let mut ms: Array1<f64> = Array::zeros(N_PLOT);
    let (tx, rx) = mpsc::channel::<(usize, f64, std::time::Duration)>();
    let mut handles = Vec::new();

    for (i, h) in hs.iter().enumerate() {
        let mut npars = PARS.clone();
        npars.h_over_j = *h;
        let nrt_vars = RT_VARS.clone();
        let mut system = model::IsingModelMC1D::new(&npars)?;

        let tx1 = mpsc::Sender::clone(&tx);
        handles.push(thread::spawn(move ||
        {
            let start = Instant::now();
            system.run(&nrt_vars, &npars);
            tx1.send((i, system.mean_m(), start.elapsed())).unwrap()
        }));
    }


    let mut ctr = 0;
    for (i, m, time) in rx {
        ctr += 1;
        ms[i] = m;
        if RT_VARS.debug_level >= 1{
            println!("{}: Thread {} finished after {:.2?}", ctr, i, time);
        }
        if ctr == N_PLOT {
            break
        }
    }

    let path_out = format!("m-{}.dat", PARS.system_size);
    arrays_to_file(&path_out, &hs, &ms);
    Ok(())
}

pub fn magnetization_of_t() -> Result<(), &'static str> {
    const N_PLOT: usize = 50;

    const RT_VARS: RunTimeVars = RunTimeVars {
        debug_level: 1,
        n_sweeps: 2_000_000
    };

    static PARS: Params = Params {
        system_size: 10,
        h_over_j: 0.0,
        init_strategy: "random",
        boundary_conditions: "pbc",
        t: 1.0,
        i_stats: 10,
        i_stats_out: 0
    };

    let ts: Array1<f64> = Array::linspace(1e-9, 1., N_PLOT);
    let mut ms: Array1<f64> = Array::zeros(N_PLOT);
    let (tx, rx) = mpsc::channel::<(usize, f64, std::time::Duration)>();
    let mut handles = Vec::new();
    for (i, t) in ts.iter().enumerate() {
        let mut npars = PARS.clone();
        npars.t = t.clone();
        let nrt_vars = RT_VARS.clone();
        let mut system = model::IsingModelMC1D::new(&npars)?;

        let tx1 = mpsc::Sender::clone(&tx);
        handles.push(thread::spawn(move ||
        {
            let start = Instant::now();
            system.run(&nrt_vars, &npars);
            tx1.send((i, system.mean_m(), start.elapsed())).unwrap();
        }));
    }


    let mut ctr = 0;
    for (i, m, time) in rx {
        ctr += 1;
        ms[i] = m.abs();
        if RT_VARS.debug_level >= 1{
            println!("{}: Thread {} finished after {:.2?}", ctr, i, time);
        }
        if ctr == N_PLOT {
            break
        }
    }

    let path_out = format!("mt-{}.dat", PARS.system_size);
    arrays_to_file(&path_out, &ts, &ms);
    Ok(())
}

pub fn correlation() -> Result<(), &'static str> {
    const RT_VARS: RunTimeVars = RunTimeVars {
        debug_level: 3,
        n_sweeps: 20_000_000
    };

    static PARS: Params = Params {
        system_size: 100,
        h_over_j: 0.0,
        init_strategy: "random",
        boundary_conditions: "free",
        t: 1.0,
        i_stats: 10,
        i_stats_out: 500_000
    };

    // init simulation
    let mut system = model::IsingModelMC1D::new(&PARS)?;
    println!("{}", system);

    // run the simulation
    system.run(&RT_VARS, &PARS);
    println!("{}", system);

    // calculate the autocorrelation
    let auto_correlation = system.autocorrelation();
    let timeseries: Array1<usize> = Array1::from(system.get_t_timeseries());
    let path = format!("autocorrelation-{}.dat", PARS.system_size);
    arrays_to_file(&path, &timeseries, &auto_correlation);

    // calculate the spin-correlation
    let s_corr = system.get_correlation();
    let i_min_j: Array1<f64> = Array1::range(0., PARS.system_size as f64, 1.);

    let path = format!("correlation-{}.dat", PARS.system_size);
    arrays_to_file(&path, &i_min_j, &s_corr);
    Ok(())

}

pub fn correlation_of_h() -> Result<(), &'static str> {
    const RT_VARS: RunTimeVars = RunTimeVars {
        debug_level: 0,
        n_sweeps: 10_000_000
    };

    static PARS: Params = Params {
        system_size: 30,
        h_over_j: 0.0,
        init_strategy: "random",
        boundary_conditions: "free",
        t: 1.0,
        i_stats: 10,
        i_stats_out: 0
    };

    let hs: [f64; 5] = [0., 0.25, 0.5, 0.75, 1.];
    for (i, h) in hs.iter().enumerate() {
        let mut pars = PARS.clone();
        println!("{}", h);
        pars.h_over_j = *h;
        let mut system = model::IsingModelMC1D::new(&pars)?;
        //println!("{}", system);

        // run the simulation
        system.run(&RT_VARS, &pars);
        //println!("{}", system);

        // calculate the spin-correlation
        let s_corr = system.get_correlation();
        let i_min_j: Array1<f64> = Array1::range(0., PARS.system_size as f64, 1.);

        let path = format!("spin-corr-{}.dat", i);
        arrays_to_file(&path, &i_min_j, &s_corr);
    }
    Ok(())
}

pub fn spin_correlation_big() -> Result<(), &'static str> {
    const RT_VARS: RunTimeVars = RunTimeVars {
        debug_level: 0,
        n_sweeps: 100_000_000
    };

    static PARS: Params = Params {
        system_size: 10_000,
        h_over_j: 0.0,
        init_strategy: "random",
        boundary_conditions: "free",
        t: 1.0,
        i_stats: 10,
        i_stats_out: 1_000_000
    };

    let mut system = model::IsingModelMC1D::new(&PARS)?;
    system.run(&RT_VARS, &PARS);
     // calculate the spin-correlation
     let s_corr = system.get_correlation();
     let i_min_j: Array1<f64> = Array1::range(0., PARS.system_size as f64, 1.);

     let path = format!("spin-corr-{}.dat", PARS.system_size);
     arrays_to_file(&path, &i_min_j, &s_corr);
     Ok(())
}
