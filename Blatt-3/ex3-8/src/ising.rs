use serde_json::json;
use serde::{Deserialize, Serialize};
use ndarray::prelude::*;
use rand::Rng;
use crate::file_handeling::RunTimeVars;

/// Global constants
/// Sets the allowed difference between the cumulative energy and the energy
/// of the system
const ASSERT_EPS: f64 = 1e-7;

/*
/// Helper function for modulus, basically implementes python %
fn mod_pyth<T> (a: T, b: T) -> usize
where
    T: std::ops::Rem
{rand = "0.7.3"
    (((a % b) + b) % b) as usize
}
*/
/// initializes the configuration according to different strategies
fn init_strategy(n: usize, strategy: &str) -> Array1<i8> {
    if strategy == "random" {
        let mut rnd_arr: Array1<i8> = Array::zeros(n);
        for i in 0..rnd_arr.len() {
            rnd_arr[i] = 2 * rand::thread_rng().gen_range(0, 2) - 1;
        }
        rnd_arr
    }
    else if strategy == "all_up" {
        Array::ones(n)
    }
    else if strategy == "all_down" {
        -Array::ones(n)
    }
    else {
        panic!("Unknown strategy in init_config")
    }
}

/// function that maps spins as +-1 to just +-
fn repr_spin(spin: &i8) -> char {
    if spin == &1 {
        '+'
    }
    else if spin == &-1 {
        '-'
    }
    else {
        panic!("spin can only be up or down")
    }
}

/// holds simulation parameters
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Params {
    pub system_size: usize,
    pub init_strategy: &'static str,
    pub boundary_conditions: &'static str,
    pub h_over_j: f64,
    pub t: f64,
    pub i_stats: usize,
    pub i_stats_out: usize
}

pub mod model {
    #[cfg(test)]
    mod tests {
        use super::*;
        #[test]
        fn test_nn() {
            let mut system = model::IsingModelMC1D{
                n: 10,
                pbc: true,
                ..Default::default()
            };
            let nn = system.get_neighbors(2);
            assert_eq!(nn, vec![1, 3]);
            let nn = system.get_neighbors(0);
            assert_eq!(nn, vec![9, 1]);
            let nn = system.get_neighbors(9);
            assert_eq!(nn, vec![8, 0]);

            system.pbc = false;
            let nn = system.get_neighbors(2);
            assert_eq!(nn, vec![1, 3]);
            let nn = system.get_neighbors(0);
            assert_eq!(nn, vec![1]);
            let nn = system.get_neighbors(9);
            assert_eq!(nn, vec![8]);
        }
    }

    use super::*;
    #[derive(Default)]
    pub struct IsingModelMC1D {
        // parameters
        n: usize,
        pbc: bool,
        pub h_over_j: f64,
        t: f64,
        beta: f64,

        // simulation variables
        config: Array1<i8>,
        correlation: Array1<isize>,
        correlation_sum: usize,

        n_attempted: usize,
        n_accepted: usize,
        pub t_timeseries: Vec<usize>,

        // statistical variables
        pub energy: f64,
        energy_sum: f64,
        energy_sum_samples: usize,
        energy_timeseries: Vec<f64>,
        pub m: f64,
        m_sum: f64,
        m_sum_samples: usize,
        m_timeseries: Vec<f64>,
        m_increment: f64,

    }

    impl std::fmt::Display for IsingModelMC1D {
        fn fmt (&self, fmt: &mut std::fmt::Formatter) -> std::result::Result<(), std::fmt::Error> {
            let mut s = String::from("****************************************************************");
            s += "\n** Ising model MC simulation";
            s += &format!("\n- {:>11} = {}", "N", self.n);
            s += &format!("\n- {:>11} = {}", "PBC", self.pbc);
            s += &format!("\n- {:>11} = {:.2}", "h_over_j", self.h_over_j);
            s += &format!("\n- {:>11} = {:.2}", "E", self.energy);
            s += &format!("\n- {:>11} = {:.2}", "T", self.t);
            s += &format!("\n- {:>11} = {:.2}", "β", self.beta);
            s += "\n-- STATISTICS --------------------------------------------------";
            s += &format!("\n- {:>11} = {:.2}", "E", self.energy);
            s += &format!("\n- {:>11} = {:.2}", "m", self.m);
            s += &format!("\n- {:>11} = {}", "n_attempted", self.n_attempted);
            s += &format!("\n- {:>11} = {}", "n_accepted", self.n_accepted);
            s += &format!("\n- {:>11} = {:.2}",
                    "acc_ratio",
                    if self.n_attempted > 0 {self.n_accepted as f64 / self.n_attempted as f64 }
                    else {0.0}
                );
            s += "\n- CONFIG:";
            s += &format!("\n//{}//", self.config_to_s());
            s += "\n****************************************************************";
            write!(fmt, "{}", s)
        }
    }


    impl IsingModelMC1D {
        // initializes the configuration
        pub fn new(pars: &Params) -> Result<IsingModelMC1D, &'static str> {
            // parameters
            let n: usize = pars.system_size;
            let pbc = if pars.boundary_conditions == "pbc" {
                true
            }
            else if pars.boundary_conditions == "free" {
                false
            }
            else {
                return Err("Unknown boundary conditions")
            };

            let h_over_j: f64 = pars.h_over_j;
            let t: f64 = pars.t;
            if t < 0.0 {
                return Err("T must be larger than 0")
            }
            let beta = 1. / t;

            let config: Array1<i8> = init_strategy(n, pars.init_strategy);
            let correlation: Array1<isize> = Array::zeros(n);

            let mut model = IsingModelMC1D {
                n,
                pbc,
                h_over_j,
                t,
                beta,
                config,
                correlation,
                ..Default::default()
            };

            model.reset_statistics_variables();
            Ok(model)
        }

        /// Refreshes the values of all variables that are cumulatively updated while the simulation runs
        pub fn reset_statistics_variables(&mut self) {
            self.n_attempted = 0;
            self.n_accepted = 0;

            self.t_timeseries = Vec::new();

            self.energy = self.get_energy();
            self.energy_sum = 0.0;
            self.energy_sum_samples = 0;
            self.energy_timeseries = Vec::new();

            self.m = self.get_m();
            self.m_sum = 0.0;
            self.m_sum_samples = 0;
            self.m_timeseries = Vec::new();
            self.m_increment = 2.0 / self.n as f64;

            self.correlation = Array1::zeros(self.n);
            self.correlation_sum = 0;
        }

        fn config_to_s(&self) -> String {
            let conf_str = self.config.map(repr_spin);
            format!("{}", conf_str)
        }

        /// Update all variables that are cumulatively tracked while the simulation is running
        fn reset_cumulative_variables(&mut self) {
            self.m = self.get_m();
            self.energy = self.get_energy();
        }

        /// Return the contribution of spin i to the total energy
        ///
        /// Pairwise energies are split evenly between the two particles involved
        fn get_energy_single(&self, i: usize) -> f64 {
            // TODO: Ask what is up with the 0.5 here. Does not make sense in my opinion
            let nbs = self.get_neighbors(i);
            let mut sum_energy = 0.0;
            let me = self.config[i];
            for j in nbs.iter() {
                sum_energy += -0.5 * me as f64 * self.config[*j] as f64;
            }
            sum_energy += -self.h_over_j * me as f64;
            sum_energy
        }

        /// Return the total current total energy of the system
        fn get_energy(&self) -> f64 {
            let mut sum_energy = 0.0;
            for i in 0..self.n {
                sum_energy += self.get_energy_single(i);
            }
            sum_energy
        }

        /// Return the current magnetization
        pub fn get_m(&self) -> f64 {
            let mut sum_m: isize = 0;
            for i in 0..self.n {
                sum_m += self.config[i] as isize;
            }
            sum_m as f64/ self.n as f64
        }


        /// Perform one simulation sweep that consists of (number of particles) MC moves
        fn do_sweep(&mut self, debug_level: usize) {
            for i in 0..self.n {
                if debug_level >= 2 {
                    println!("* Doing move {} / {}", i+1, self.n);
                self.do_move(debug_level);
                }
            }
        }
        /// Accept a flip of spin i
        /// If debug_level >= 3 or higher a consistency check of the cumulative variables E
        /// and m is performed. If debug_level >= 4 the state of the system before the move
        /// is accepted is printed.
        fn accept_move(&mut self, i: usize ,d_energy: f64 , debug_level: usize){
            if debug_level >= 4{
                println!("{}", self);
            }
            self.n_accepted += 1;
            self.m -= self.m_increment * self.config[i] as f64;
            self.config[i] *= -1;
            self.energy += d_energy;
            if debug_level >= 3 {
                assert!((self.m - self.get_m()).abs() < ASSERT_EPS,
                    format!("inconsistent magnetization: running m = {:.8} != {:.8} = summed m", self.m,self.get_m())
                );
                assert!((self.energy - self.get_energy()).abs() < ASSERT_EPS,
                    format!("inconsistent energy: running E = {:.8} != {:.8} = summed E", self.energy, self.get_energy())
                );
            }
        }

        /// Update the statistics based on the current state of the system
        fn do_stats(&mut self, i: usize){
            self.t_timeseries.push(i);

            self.energy_sum += self.energy;
            self.energy_sum_samples += 1;
            self.energy_timeseries.push(self.energy);

            // TODO: Ask if this is a bug
            self.m_sum += self.m; // / self.n as f64;
            self.m_sum_samples += 1;
            self.m_timeseries.push(self.m);

            // aded to get the correlation function
            // TODO: Compare to analytical result
            self.correlation += &self.config.mapv(|spin_i| self.config[0] as isize * spin_i as isize);
            self.correlation_sum += 1;
        }

        /// Print a header line for the statistics output
        fn  print_stats_header(&self){
            let mut s = String::from(format!("{:10}", "t"));
            s += &format!(" | {:10}", "MAGN");
            s += &format!(" | {:10}", "AVG MAGN");
            s += &format!(" | {:10}", "ENERGY");
            s += &format!(" | {:10}", "AVG ENERGY");
            s += &format!(" | {:10}", "ACC RATIO");
            println!("{}", s);
        }

        /// Print the statistics output
        fn print_stats(&self, i: usize){
            let mut s = String::from(format!("{:10}", i));

            s += &format!(" | {:10.2}", self.m);
            let avg_m = if self.m_sum_samples > 0 {
                self.mean_m()
            }
                else {0.0};
            s += &format!(" | {:10.2}", avg_m);

            s += &format!(" | {:10.2}", self.energy);
            let avg_energy = if self.energy_sum_samples > 0 {
                self.energy_sum / self.energy_sum_samples as f64
            }
                else {0.0};
            s += &format!(" | {:10.2}", avg_energy);

            let acc_ratio = if self.n_attempted > 0 {
                self.n_accepted as f64 / self.n_attempted as f64
            }
                else {0.0};
            s += &format!(" | {:10.2}", acc_ratio);
            println!("{}", s);
        }

        /// Run a simulation
        ///
        /// n_sweeps - int: Number of sweeps to be performed
        /// pars - dict: parameters used for the simulation. Must include keys i_stats and i_stats_out
        /// debug_level - int: 0 or larger, controls level of debug output
        pub fn run(&mut self, rt_pars: &RunTimeVars, pars: &Params) {
            let debug_level: usize = rt_pars.debug_level;
            let n_sweeps: usize = rt_pars.n_sweeps;

            let i_stats = pars.i_stats;
            let i_stats_out = if i_stats > 0 {
                pars.i_stats_out
            }
                else { 0 };

            if i_stats_out > 0 {
                self.print_stats_header()
            }
            for i in 0..n_sweeps{
                self.do_move(debug_level);
                if i_stats > 0 && (i % i_stats == 0) {
                    self.do_stats(i);
                }
                if i_stats_out > 0 && (i % i_stats_out == 0) {
                    self.print_stats(i);
                }
                if i % 10_000 == 0 {
                    self.reset_cumulative_variables()
                }
            }

            if debug_level >= 2 {
                println!("*********************************");
                println!("End of simulation");
            }
        }

        /// Return timeseries tracked while the last simulation was run
        ///
        /// Returns a dictionary with the name of variable as key and numpy.ndarray as values
        pub fn get_timeseries(&self) -> serde_json::Value{
            json!({
                "t": self.t_timeseries.clone(),
                "m": self.m_timeseries.clone(),
                "E": self.energy_timeseries.clone()
            })
        }

        // --------------------- Methods to implement ---------------------

        /// Return the neighbors of spin i as an iterable (list or tuple)
        fn get_neighbors(&self, i: usize) -> Vec<usize>{
            // TODO: should only return neighbors, not the value itself
            // -- START IMPLEMENTING HERE
            if self.pbc {
                if i == 0 {
                    vec![self.n-1, i+1]
                }
                else if i == self.n-1 {
                    vec![i-1, 0]
                }
                else {
                    vec![i-1, i + 1]
                }


            }
            else {
                if i == 0 {
                    vec![i+1]
                }
                else if i == self.n-1 {
                    vec![i-1]
                }
                else {
                    vec![i-1, i+1]
                }
            }
            // -- STOP IMPLEMENTING HERE
        }


        /// Return the change in energy of particle i is flipped
        fn get_d_energy_single(&mut self, i: usize) -> f64{
            let mut d_energy = 0.0;
            // -- START IMPLEMENTING HERE
            // Calculate the change in potential energy if spin i were to be flipped
            // Use the get_neighbors function you implemented above
            let nbs = self.get_neighbors(i);
            let flipped_i: f64 = (-1 * self.config[i]).into();

            for j in nbs.iter() {
                d_energy += -2. * self.config[*j] as f64 * flipped_i;
            }
            d_energy += -2. * flipped_i * self.h_over_j;
            // -- STOP IMPLEMENTING HERE
            d_energy
        }

        /// Return true if Metropolis MC acceptance criterion is accepted - false otherwise
        fn acceptance(&self, d_energy: f64, debug_level: usize) -> bool{
        // -- START IMPLEMENTING HERE
        // Implement the Metropolis acceptance criterion. dE is the difference in potential energy
            if debug_level >= 4 {
                println!("dE = {}", d_energy);
            }
            if d_energy <= 0.0 {
                true
            }
            else {
                let prob  = (-self.beta * d_energy).exp();
                let xi: f64 = rand::thread_rng().gen_range(0., 1.);
                if xi < prob {
                    true
                }
                else {
                    false
                }
            }
        // -- STOP IMPLEMENTING HERE
        }

        /// Perform a single Metropolis MC move including an acceptance test
        fn do_move(&mut self, debug_level: usize) {
            // -- START IMPLEMENTING HERE
            let i: usize = rand::thread_rng().gen_range(0, self.n);
            let d_energy = self.get_d_energy_single(i);
            let accept = self.acceptance(d_energy, debug_level);
            // -- STOP IMPLEMENTING HERE

            self.n_attempted += 1;
            if accept{
                self.accept_move(i, d_energy, debug_level)
            }
            else{
                ()
            }
        }

        // ---------------- Convenience functions -------------------
        /// function that gives mean magnetization per particle
        pub fn mean_m(&self) -> f64 {
            self.m_sum / self.m_sum_samples as f64
        }

        /// Calculate the autocorrelation of the m-timeseries
        // TODO: Can the correlation be negative?
        pub fn autocorrelation(&self) -> Array1<f64> {
            let tmax: usize = self.t_timeseries.len();
            let m_series: Array1<f64> = Array1::from(self.m_timeseries.clone());
            let sigma_m: f64 = m_series.std_axis(Axis(0), 0.0).into_scalar();
            println!("the full variance is {}", sigma_m);
            let mut cm: Array1<f64> = Array1::zeros(tmax);
            for k in 0..tmax {
                print!("k:{}\r", k);
                let mut prod_m = 0.0;
                let mut sum_mt = 0.0;
                let mut sum_mk = 0.0;
                for t in 0..(tmax - k) {

                    prod_m += m_series[t] * m_series[t + k];
                    sum_mt += m_series[t];
                    sum_mk += m_series[t+k];
                }
                cm[k] = (prod_m - sum_mt * sum_mk / (tmax - k) as f64) /(sigma_m * (tmax - k) as f64);
            }
            cm
        }

        /// gets the correlation vector
        pub fn get_correlation(&self) -> Array1<f64> {
            self.correlation.mapv(|corr| corr as f64 / self.correlation_sum as f64)
        }

        /// gives timeseries as a vector
        pub fn get_t_timeseries(&self) -> Array1<usize> {
            Array1::from(self.t_timeseries.clone())
        }

        /// gives acceptance rate
        pub fn get_acceptance_rate(&self) -> f64 {
            self.n_accepted as f64 / self.n_attempted as f64
        }
    }
}