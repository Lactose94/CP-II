mod file_handeling;
mod ising;
mod simulation;

#[macro_use]
extern crate serde_json;
use std::env;
use std::process;


fn main() {
    let cmd_params: Vec<String> = env::args().collect();

    let rt_vars = file_handeling::init_rt_vars(&cmd_params);

    /*let mut pars: Params = file_handeling::read_params(&PATH).unwrap_or_else(
        |err| {
            eprintln!("{}", err);
            process::exit(1)
        }
    ); */

    /*simulation::magnetization_of_h()
        .unwrap_or_else(|err| {
            eprintln!("{}", err);
            process::exit(1);
        }
    ); */

    /*simulation::magnetization_of_t()
        .unwrap_or_else(|err| {
            eprintln!("{}", err);
            process::exit(1);
        }
    );*/

   /* simulation::correlation().unwrap_or_else(
        |err| {
            eprintln!("{}", err);
            process::exit(1)
        }
        ); */
    simulation::spin_correlation_big().unwrap_or_else(
        |err| {
            eprintln!("{}", err);
            process::exit(1)
        }
    );

    simulation::correlation_of_h().unwrap_or_else(
        |err| {
            eprintln!("{}", err);
            process::exit(1)
        }
    );
}
